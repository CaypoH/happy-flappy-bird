from kivy.app import App
from kivy.uix.modalview import ModalView


class QuitPopup(ModalView):
    def quit_game(self):
        App.get_running_app().stop()