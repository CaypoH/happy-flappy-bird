import random
from coin import Coin
from pipe import Pipe
from os import listdir
from enemy import Enemy
from constants import *
from cactus import Cactus
from kivy.clock import Clock
from os.path import isfile, join
from kivy.lang import Builder
from kivy.loader import Loader
from kivy.uix.image import Image
from kivy.config import ConfigParser
from kivy.animation import Animation
from kivy.core.audio import SoundLoader
from kivy.uix.screenmanager import Screen
from kivy.graphics.vertex_instructions import Rectangle
from kivy.utils import platform
if platform=='android':
    from jnius import autoclass
    PythonActivity=autoclass("org.kivy.android.PythonActivity")
    AdBuddiz=autoclass("com.purplebrain.adbuddiz.sdk.AdBuddiz")
    AdBuddiz.setPublisherKey(PUBLISHER_KEY)
    AdBuddiz.setTestModeActive()
    AdBuddiz.cacheAds(PythonActivity.mActivity)


Builder.load_file('pipe.kv')
Builder.load_file('bird.kv')
Builder.load_file('coin.kv')
Builder.load_file('enemy.kv')
Builder.load_file('cactus.kv')
Builder.load_file('gameoverpopup.kv')
Builder.load_file('pausepopup.kv')


class GameScreen(Screen):
    def __init__(self, **kwargs):
        super(GameScreen, self).__init__(**kwargs)
        self.pipes_rate = 180
        self.enemies_rate = 475
        self.score = 0
        self.frames = 0
        self.velocity = 0
        self.gravity = .8
        self.jump = self.gravity * 40
        self.pipes_speed = 5.68
        self.pipes_gap = .3
        self.pipe_width = .2
        self.coin_width = .064
        self.coin_height = .0423529411765
        self.game_on = False
        self.conf = ConfigParser()
        self.conf.read(CONFIG_FILE)
        self.hit_sound = SoundLoader.load(SOUNDS_FOLDER + HIT_SOUND)
        self.hit_sound.seek(0)
        self.point_sound = SoundLoader.load(SOUNDS_FOLDER + POINT_SOUND)
        self.point_sound.seek(0)
        self.coin_sound = SoundLoader.load(SOUNDS_FOLDER + COIN_SOUND)
        self.coin_sound.seek(0)
        self.coins_collected = self.conf.getint('game', 'coins')
        self.sound_on = self.conf.getint('sounds', 'sounds')
        # the following is a workaround to play multiple tap sound simultaneously
        self.tap_sound = [SoundLoader.load(SOUNDS_FOLDER + TAP_SOUND) for i in range(5)]
        self.tap_sound_index = 0
        # following fired if ads is showed. Used in main to close pause popup automatically after the ad was shown
        self.ad_showed = False
        for sound in range(len(self.tap_sound)):
            self.tap_sound[sound].seek(0)
        # end of workaround
        # preloading all images
        images = [f for f in listdir(IMG_FOLDER) if isfile(join(IMG_FOLDER, f))]
        for img in images:
            Loader.image(IMG_FOLDER + img)

    def on_pre_enter(self, *args):
        # I feel shame for the following every day of my life
        self.gamearea.add_widget(Pipe(id='preload',pos=(self.x-1000,self.height + 1),source=IMG_FOLDER + 'pipe_bot_1.png'))
        self.gamearea.add_widget(Pipe(id='preload',pos=(self.x-1000,self.height + 1),source=IMG_FOLDER + 'pipe_bot_2.png'))
        self.gamearea.add_widget(Pipe(id='preload',pos=(self.x-1000,self.height + 1),source=IMG_FOLDER + 'pipe_bot_3.png'))
        self.gamearea.add_widget(Pipe(id='preload',pos=(self.x-1000,self.height + 1),source=IMG_FOLDER + 'pipe_bot_4.png'))
        self.gamearea.add_widget(Pipe(id='preload',pos=(self.x-1000,self.height + 1),source=IMG_FOLDER + 'pipe_bot_5.png'))
        self.gamearea.add_widget(Pipe(id='preload',pos=(self.x-1000,self.height + 1),source=IMG_FOLDER + 'pipe_bot_6.png'))
        self.gamearea.add_widget(Pipe(id='preload',pos=(self.x-1000,self.height + 1),source=IMG_FOLDER + 'pipe_top_0.1.png'))
        self.gamearea.add_widget(Pipe(id='preload',pos=(self.x-1000,self.height + 1),source=IMG_FOLDER + 'pipe_top_0.2.png'))
        self.gamearea.add_widget(Pipe(id='preload',pos=(self.x-1000,self.height + 1),source=IMG_FOLDER + 'pipe_top_0.3.png'))
        self.gamearea.add_widget(Pipe(id='preload',pos=(self.x-1000,self.height + 1),source=IMG_FOLDER + 'pipe_top_0.4.png'))
        self.gamearea.add_widget(Pipe(id='preload',pos=(self.x-1000,self.height + 1),source=IMG_FOLDER + 'pipe_top_0.5.png'))
        self.gamearea.add_widget(Pipe(id='preload',pos=(self.x-1000,self.height + 1),source=IMG_FOLDER + 'pipe_top_0.6.png'))
        self.gamearea.add_widget(Coin(id='preload',pos=(self.x-1000,self.height + 1)))
        self.gamearea.add_widget(Enemy(id='preload',pos=(self.x-1000,self.height + 1)))
        self.gamearea.add_widget(Cactus(id='preload',pos=(self.x-1000,self.height + 1)))

        self.coinz.text = str(self.coins_collected)
        self.gravity = float(self.height) / 1600
        self.jump = self.gravity * 40
        self.pipes_speed = self.width * .0055555
        if self.sound_on:
            self.soundbutton.state = 'normal'
        else:
            self.soundbutton.state = 'down'

    def on_enter(self, *args):
        with self.canvas:
            self.weed.texture.wrap = 'repeat'
            self.grass = Rectangle(texture=self.weed.texture, size=self.weed.size, pos=self.weed.pos)

    def start_game(self):
        self.conf.read(CONFIG_FILE)
        self.gameover.dismiss()
        self.gamearea.clear_widgets()
        self.gamearea.add_widget(self.bird)
        self.gamearea.add_widget(self.intro)
        self.gamearea.add_widget(self.coinz)
        self.gamearea.add_widget(self.scorez)
        self.gamearea.add_widget(self.soundbutton)
        self.gamearea.add_widget(self.coinimg)
        self.bird.y = self.center_y
        self.pipes_speed = self.width * .0055555
        self.frames = 0
        self.velocity = 0
        self.coins_collected = self.conf.getint('game', 'coins')
        self.character = self.conf.get('game', 'character')
        self.score = 0
        self.scorez.text = str(0)
        self.bird.source = IMG_FOLDER + self.character + BIRD_IMG
        self.game_on = False
        self.sound_on = self.conf.getint('sounds', 'sounds')
        self.clock = Clock.schedule_interval(self.update, 1 / 60)
        self.manager.current = self.name

    def pause(self):
        self.pausepopup.open()
        self.clock.cancel()

    def unpause(self):
        self.pausepopup.dismiss()
        self.clock()

    def score(self):
        return self.score

    def best_score(self):
        return self.conf.getint('game', 'best_score')

    def ad_status(self):
        return self.ad_showed

    def end_game(self):
        if platform == 'android':
            if random.randint(0, 2) == 1:  # we don't want an ads to be annoying
                self.ad_showed = True
                AdBuddiz.showAd(PythonActivity.mActivity)
            else:
                self.ad_showed = False
        if self.sound_on:
            self.hit_sound.play()
        if int(self.score) > self.best_score():
            self.conf.set('game', 'best_score', int(self.score))
            self.conf.write()
            self.gameover.new_best.add_widget(Image(id='new_best',
                                                    source=IMG_FOLDER+'new_best.png',
                                                    pos_hint={'center_x': .5, 'y': .3},
                                                    size_hint=[.5, .2]))
        else:
            self.gameover.new_best.clear_widgets()
        self.gameover.score.text = 'Score: ' + str(int(self.score))
        self.gameover.best_score.text = 'Best: ' + str(self.best_score())
        self.gameover.open()

    def on_touch_down(self, touch):
        # if game is not started yet first tap will begin a game
        if not self.game_on and not self.soundbutton.collide_point(touch.x, touch.y):
            self.gamearea.remove_widget(self.intro)
            self.game_on = True
        if self.sound_on:
            self.tap_sound[self.tap_sound_index].play()
            self.tap_sound[self.tap_sound_index].seek(0)
            self.tap_sound_index = (self.tap_sound_index + 1) % len(self.tap_sound)
        # if player pressed a pause button at the top right corner we pause a game
        if self.soundbutton.collide_point(touch.x, touch.y):
            if self.sound_on:
                self.soundbutton.state = 'down'
                self.sound_on = False
                self.conf.set('sounds', 'sounds', 0)
                self.conf.write()
            else:
                self.soundbutton.state = 'normal'
                self.sound_on = True
                self.conf.set('sounds', 'sounds', 1)
                self.conf.write()
        # if he didn't we 'jump' a bird. It's a workaround to prevent a bird from jumping if player taped a pause button
        else:
            self.velocity -= self.jump

    def update(self, dt):
        if self.game_on:
            self.frames += 1
            self.velocity += self.gravity
            self.velocity *= .98
            self.bird.y -= self.velocity
            self.coinz.text = str(self.coins_collected)
            t = Clock.get_boottime()
            self.grass.tex_coords = -(t * 0.35), 0, -(t * 0.35 + 1), 0, -(t * 0.35 + 1), -1, -(t * 0.35), -1

            # check if bird hits top of the screen and prevent it from going outside of it<
            if self.bird.y + self.bird.height >= self.height:
                self.bird.y = self.height - self.bird.height
            # check if bird hits bottom of the screen and count it as gameover
            if self.bird.y <= self.gamearea.y:
                self.bird.source=IMG_FOLDER + self.character + BIRD_HIT_IMG
                self.end_game()
                return False

            # every self.rate frame add 2 pipes and a coin to a screen
            if self.frames % self.pipes_rate == 0:
                bot_pipe_height = float(random.randrange(1,7)) / 10
                top_pipe_height = 1 - bot_pipe_height - self.pipes_gap
                self.gamearea.add_widget(Pipe(id='pipe' + str(self.frames),
                                              size_hint=[self.pipe_width,bot_pipe_height],
                                              pos=(self.width,self.gamearea.y),
                                              source=IMG_FOLDER + 'pipe_bot_' + str(int(bot_pipe_height * 10)) + '.png'), index=5)
                self.gamearea.add_widget(Pipe(id='pipe' + str(self.frames - 1),
                                              size_hint=[self.pipe_width, top_pipe_height],
                                              pos=(self.width,
                                                   self.gamearea.y + self.gamearea.height * bot_pipe_height + self.gamearea.height * self.pipes_gap),
                                              source=IMG_FOLDER + 'pipe_top_' + str(top_pipe_height) + '.png'), index=5)
                self.gamearea.add_widget(Coin(id='coin',
                                              size_hint=[self.coin_width, self.coin_height],
                                              x=(self.width + self.width * self.pipe_width / 2) - self.width * self.coin_width / 2,
                                              y=self.gamearea.y + self.gamearea.height * bot_pipe_height + self.height * (self.coin_height / 2)), index=5)

            if self.frames % self.pipes_rate == self.pipes_rate / 2 and self.score > 5:
                self.gamearea.add_widget(Cactus(id='cactus',
                                                x=self.width,
                                                y=self.gamearea.y), index=5)

            if self.frames % self.enemies_rate == 0 and self.score > 10:
                self.gamearea.add_widget(Enemy(id='enemy',
                                               x=self.width,
                                               y=random.randrange(self.gamearea.y, self.height)), index=5)
                if self.score > 30:
                    self.gamearea.add_widget(Enemy(id='enemy',
                                                   x=self.width,
                                                   y=random.randrange(self.gamearea.y, self.height)), index=5)

            for widget in self.gamearea.children:
                if widget.id: # bird have an id 'None'
                    widget.x -= self.pipes_speed # moving all widgets from right to left

                    if widget.id[:5] == 'enemy':
                        widget.x -= self.pipes_speed # enemy is moving twice as fast as pipes does

                    # check if bird has crashed into something but not into coin
                    if widget.collide_widget(self.bird) and widget.id[:4] != 'coin':
                        self.bird.source = IMG_FOLDER + self.character + BIRD_HIT_IMG
                        self.end_game()
                        return False

                    # check if bird has collected a coin
                    if widget.collide_widget(self.bird) and widget.id == 'coin':
                        widget.id = 'coin_picked'
                        self.coins_collected += 1
                        self.conf.set('game', 'coins', self.coins_collected)
                        self.conf.write()
                        anim = Animation(x=self.x - widget.width, y=self.height, duration=.3)
                        anim.start(widget)
                        if self.sound_on:
                            self.coin_sound.play()
                            self.coin_sound.seek(0)

                    # check if coin is at the top left corner and remove it if yes
                    if widget.pos == self.coinz.pos:
                        self.gamearea.remove_widget(widget)

                    if widget.x + widget.width < 0: # if widget is off the screen - remove it so we save resources
                        self.gamearea.remove_widget(widget)
                        if widget.id[:4] == 'pipe':
                            self.score += .5
                            self.scorez.text = str(int(self.score))
                            if self.sound_on and int(self.score % 1) == 0:
                                self.point_sound.play()
                                self.point_sound.seek(0)
                        if self.score == 15:
                            self.pipes_speed *= 1.1

        else:
            pass