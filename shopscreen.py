from kivy.lang import Builder
from kivy.uix.screenmanager import Screen
from kivy.uix.button import Button
from kivy.uix.togglebutton import ToggleButton
from kivy.properties import NumericProperty
from constants import *


Builder.load_file('buypopup.kv')


class CharButton(Button):
    char_price = NumericProperty()

    def __init__(self, **kwargs):
        super(CharButton, self).__init__(**kwargs)
        self.border = [0,0,0,0]

class ShopScreen(Screen):
    def __init__(self, **kwargs):
        super(ShopScreen, self).__init__(**kwargs)
        self.conf = ConfigParser()
        self.conf.read(CONFIG_FILE)
        self.character = self.conf.get('game', 'character')
        self.characters = {'duck': self.conf.getint('characters', 'duck'),
                           'chicken': self.conf.getint('characters', 'chicken'),
                           'bird': self.conf.getint('characters', 'bird'),
                           'monster': self.conf.getint('characters', 'monster'),
                           'helmet_bird': self.conf.getint('characters', 'helmet_bird')}
        self.character_prices = {'duck': 500,
                                 'chicken': 800,
                                 'bird': 1000,
                                 'monster': 2000,
                                 'helmet_bird': 2500}

    def on_pre_enter(self, *args):
        self.conf.read(CONFIG_FILE)
        self.character = self.conf.get('game', 'character')
        self.characters = {'duck': self.conf.getint('characters', 'duck'),
                           'chicken': self.conf.getint('characters', 'chicken'),
                           'bird': self.conf.getint('characters', 'bird'),
                           'monster': self.conf.getint('characters', 'monster'),
                           'helmet_bird': self.conf.getint('characters', 'helmet_bird')}
        self.chars.clear_widgets()
        self.shop.clear_widgets()
        for key, value in self.characters.iteritems():
            if value:
                if key == self.character:
                    self.chars.add_widget(ToggleButton(id=key,
                                                       background_normal=IMG_FOLDER + key + '.png',
                                                       background_down=IMG_FOLDER + key + '_checked.png',
                                                       on_press=self.set_character,
                                                       group='char',
                                                       state='down',
                                                       border=[0,0,0,0]))
                else:
                    self.chars.add_widget(ToggleButton(id=key,
                                                       background_normal=IMG_FOLDER + key + '.png',
                                                       background_down=IMG_FOLDER + key + '_checked.png',
                                                       on_press=self.set_character,
                                                       group='char',
                                                       state='normal',
                                                       border=[0, 0, 0, 0]))
            else:
                self.shop.add_widget(CharButton(id=key,
                                                background_normal=IMG_FOLDER + key + '_disabled.png',
                                                background_down=IMG_FOLDER + key + '_disabled.png',
                                                char_price=self.character_prices[key],
                                                on_press=self.buy_window))
        if len(self.chars.children) > 3:
            self.chars.size_hint_y = self.shop.size_hint_y = .4

    def open(self):
        self.manager.current = self.name

    def buy_window(self, instance):
        self.conf.read(CONFIG_FILE)
        if self.conf.getint('game', 'coins') >= instance.char_price:
            if len(self.buy.buyscreen.children) < 3:
                self.buy.cancel.text = 'CANCEL'
                self.buy.cancel.pos_hint = {'x': .05}
                self.buy.buyscreen.add_widget(self.buy.buybutton)
            self.buy.descr.text = 'It will cost you [color=df002f]' + str(instance.char_price)
            self.buy.descr.text += '[/color] coins. Do you want to unlock a character?'
            self.buy.buybutton.char = instance.id
        else:
            self.buy.descr.text = 'You need [color=df002f]' + str(instance.char_price) + '[/color] coins to unlock this'
            self.buy.descr.text += ' character. You only have [color=df002f]'
            self.buy.descr.text += str(self.conf.getint('game', 'coins')) + '[/color]'
            self.buy.cancel.text = 'OK'
            self.buy.cancel.pos_hint = {'center_x': .5}
            self.buy.buyscreen.remove_widget(self.buy.buybutton)
        self.buy.open()

    def set_character(self, instance):
        self.conf.set('game', 'character', instance.id)
        self.conf.write()

    def buy_char(self, char):
        for child in self.shop.children:
            if child.id == char:
                self.conf.set('game', 'coins', self.conf.getint('game', 'coins') - child.char_price)
                self.conf.set('characters', child.id, 1)
                self.conf.write()
                self.shop.remove_widget(child)
                self.chars.add_widget(ToggleButton(id=child.id,
                                                   background_normal=IMG_FOLDER + child.id + '.png',
                                                   background_down=IMG_FOLDER + child.id + '_checked.png',
                                                   on_press=self.set_character,
                                                   group='char',
                                                   state='normal',
                                                   border=[0, 0, 0, 0]))
                self.buy.dismiss()

        if len(self.chars.children) > 3:
            self.chars.size_hint_y = self.shop.size_hint_y = .4