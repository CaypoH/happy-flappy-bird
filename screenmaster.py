from kivy.uix.screenmanager import ScreenManager


class ScreenMaster(ScreenManager):
    def get_current_screen(self):
        return self.current