from kivy.uix.modalview import ModalView


class PausePopup(ModalView):
    def go_to_start(self):
        self.dismiss()
        self.startscreen.open()