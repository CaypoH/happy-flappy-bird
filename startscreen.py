from kivy.lang import Builder
from kivy.uix.screenmanager import Screen

Builder.load_file('quitpopup.kv')

class StartScreen(Screen):
    def open(self):
        self.manager.current = self.name