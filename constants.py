from kivy.config import ConfigParser
# game config file
CONFIG_FILE='flappy.ini'

conf = ConfigParser()
conf.read(CONFIG_FILE)

# folder with game images
IMG_FOLDER='images/'

# this is not actually a constant. We are changing it in settings.
# this can be: duck, bird, chicken, monster, helmet_bird
CHARACTER=conf.get('game', 'character')

# background image. Must be inside of IMG_FOLDER
BACKGROUND_IMG='background.png'

# transparent pixel for a transparent background
TRANSPARENT_BACKGROUND='transparent_background.png'

# bird animation. Must be inside of IMG_FOLDER
BIRD_IMG='_fly.zip'

# animation of the bird if it hits something
BIRD_HIT_IMG='_hit.zip'

# animation of the coin. Must be inside of IMG_FOLDER
COIN_IMG='coin.zip'

# background image of the gamearea
SKY_IMG='sky.png'

# background image of the bottom area
WEED_IMG='weed.png'

# image of the enemy. Must be inside IMG_FOLDER
ENEMY_IMG='enemy.zip'

# image of the cactus. Must be inside IMG_FOLDER
CACTUS_IMG='cactus.png'

# folder with game fonts
FONTS_FOLDER='fonts/'

# main font. Must be inside of FONTS_FOLDER
MAIN_FONT='chargen.ttf'

# font for gameover text. Must be inside of FONTS_FOLDER
GAMEOVER_FONT='joystix.ttf'

# folder with game sounds
SOUNDS_FOLDER='sounds/'

# tap sound
TAP_SOUND='wing.ogg'

# sound of hitting something
HIT_SOUND='hit.ogg'

# new point sound
POINT_SOUND='point.ogg'

# sound of collecting a coin
COIN_SOUND='coin.ogg'

# AdBuddiz publisher key
PUBLISHER_KEY = 'f3a31e99-7261-4e80-9994-6eaa247eea51'