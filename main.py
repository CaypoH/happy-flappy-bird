from kivy.app import App
from kivy.lang import Builder
from screenmaster import ScreenMaster
from kivy.base import EventLoop


Builder.load_file('startscreen.kv')
Builder.load_file('gamescreen.kv')
Builder.load_file('shopscreen.kv')


class MainApp(App):
    def on_pause(self):
        if self.manager.get_current_screen() == 'game':
            self.manager.game.pause()
        return True

    def on_resume(self):
        if self.manager.game.ad_status():
            self.manager.game.pausepopup.dismiss()
        return True

    def build(self):
        sm = ScreenMaster()
        self.manager = sm
        return sm

    def on_start(self):
        EventLoop.window.bind(on_keyboard=self.hook_keyboard)

    def hook_keyboard(self, window, key, *largs):
        if key == 27:
            if self.manager.get_current_screen() == 'game':
                self.manager.game.pause()
                return True
            if self.manager.get_current_screen() == 'shop':
                self.manager.current = 'start'
                return True
            if self.manager.get_current_screen() == 'start':
                self.manager.start.quit.open()
                return True
            return True

if __name__ == '__main__':
    MainApp().run()